![效果演示](asset/output.gif)

本项目是集群聊天服务器的客户端，基于Qt6、QML和FluentUI开发

对本项目的演示，请看视频[【集群聊天服务器与仿微信客户端开发，服务器基于muduo，mysql，redis，客户端基于Qt6和FluentUI，代码已开源】 ](https://www.bilibili.com/video/BV1ZJ4m1L7ur/?share_source=copy_web&vd_source=9ae54c31182cbbfa8dbdc757855a2545)

服务端的代码见https://gitee.com/ericling666/sponge

页面参考了[https://github.com/zhuzichu520/kim-qt](https://github.com/zhuzichu520/kim-qt)的代码，该项目是FluentUI的作者编写的，仅开源了客户端的代码，使用的是websocket，服务端使用netty和java实现。

本项目的客户端基本仿照他的kim-qt代码实现的。
# 页面展示

登录页面

![登录页面](asset/2024-03-26_23-05.png)

注册页面

![注册页面](asset/2024-03-26_23-19.png)

主页面

![主页面](asset/2024-03-26_23-20.png)

创建群聊

![创建群聊](asset/2024-03-26_23-21.png)

发送表情包

![发送表情包](asset/2024-03-26_23-23.png)

好友列表

![好友列表](asset/2024-03-26_23-24.png)

添加好友

![添加好友](asset/2024-03-26_23-25.png)


# 如何使用

首先需要启动服务端，服务端的部署比较简单，见https://gitee.com/ericling666/sponge

客户端的编译，需要用到Qt6,在windows和linux mint中都编译过，仅测试过6.2.4和6.6.2两个版本，其它Qt6的版本应该也可以，没有试过Qt5版本

首先下载本仓库
```shell
git clone https://gitee.com/ericling666/spongeclient
```
克隆到本地之后，还需要下载依赖库，在spongeclient目录下执行以下命令
```shell
git clone https://github.com/zhuzichu520/fluentui
```

然后用qt creator打开， 进行编译即可

在首次使用时，需要在设置中修改服务器的ip地址和端口号

当然也可以在[配置文件](src/res/server.ini)中修改

# 一些细节

服务器与客户端通信是直接使用的tcp长连接，服务端使用muduo网络库，客户端使用QTcpSocket，网络断开之后会自动重连，通过QTimer定时发起连接，连接建立之后，自动重新登录。

发送的数据是用json格式进行包装，通过https://github.com/nlohmann/json 第三方库进行解析。

一些json数据格式样例如下：
```json
// 用户登录
{"msgType":1, "USERID":6, "password":"6"}
// 注册用户
{"msgType":2, "name":"Eric Ling", "password":"whatever"}
// 退出登录
{"msgType":3, "USERID":7}
// 用户聊天
{"msgType":4,"FROMID":6,"TOID":8,"msg":"你好 8号","sendTime":1703078264002}
// 添加好友
{"msgType":6,"U1":1,"U2":2}
// 查找好友
{"msgType":8,"FROMID":6}
// 群聊消息
{"msgType":9,"FROMID":6,"GROUPID":8,"msg":"你好 8号","sendTime":1703078264002}
// 加入群聊
{"msgType":10,"FROMID":6,"GROUPID":7}
// 创建群聊
{"msgType":12,"FROMID":6,"name":"cpp","description":"cpp group."}
// 查询离线消息
{"msgType":14,"FROMID":1}
// 查询账号
{"msgType":15,"keyword":"1"}
```

客户端离线消息的存储则是用sqlite，使用Qt封装的QSqlQuery可以很方便的执行一些增删改查操作。

通过共享内存实现单例启动，具体代码见[singleinstancechecker](src/singleinstancechecker.h)

一些配置则是用QSettings和QVariant存储到ini文件中。

更多细节请看服务端https://gitee.com/ericling666/sponge