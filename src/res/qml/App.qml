import QtQuick
import FluentUI

Item {
    id: app

    Component.onCompleted: {
        FluApp.init(app)
        FluApp.vsync = true
        FluApp.routes = {
            "/login":"qrc:/spongeclient/res/qml/window/LoginWindow.qml",
            "/setting":"qrc:/spongeclient/res/qml/window/SettingWindow.qml",
            "/":"qrc:/spongeclient/res/qml/window/MainWindow.qml",
        }
        if(SettingsManager.isLogin()){
            FluApp.initialRoute = "/"
        }else{
            FluApp.initialRoute = "/login"
        }
        FluApp.run()
    }
}
